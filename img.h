typedef signed      char        INT8;
typedef unsigned    char        UINT8;
typedef signed      short       INT16;
typedef unsigned    short       UINT16;
typedef signed      int         INT32;
typedef unsigned    int         UINT32;

typedef struct
{
	unsigned int size;			/* binary size */
	unsigned int chksum;		/* binary chksum */
	unsigned int start_block;	/* start block in flash */
	unsigned int block_size;	/* required block sizes */
}BINHDR, *PBINHDR;
typedef struct
{
	unsigned int ID;			/* program ID */   //  "INC"
	unsigned int langID;		/* language ID */
	unsigned char version[8];
	unsigned int date;			/* yyyy/mm/dd  b3b2/b1/b0 */
	unsigned int time;			/* hhhh:mm:ss  b3b2/b1/b0 */
	unsigned int size;			/* total size of all binary files, except this header */
	unsigned int chksum;		/* chksum of all binary files, except this header */
	BINHDR	osbin;				/* os binary file header */
	BINHDR	romfsbin;			/* romfs binary file header */
	BINHDR	flashfsbin;			/* flashfs binary file header */
}PRGHDR, *PPRGHDR;


typedef struct{
         unsigned int type;              /* binary type  1-Boot1, 2-Boot2, 3-MainCode, 4-GSM module*/
         unsigned int size;              /* binary size */
         unsigned char version[8];       /* binary version */
}IACBINHDR, *PIACBINHDR;

typedef struct{
         unsigned int ID;              /* program ID, current is "IAC"  */
         unsigned int size;                     /* total size of all binary files, except this header */
         unsigned int chksum;              /* chksum of all binary files, except this header */
     	unsigned char version[6];       /* version of this */
     	unsigned char flag1;                           /* flag1 of this */
     	unsigned char flag2;                           /* flag2 of this */

         unsigned int count;                   /* binary file count */
//         PIACBINHDR binaries;            /*  binary file header */
}IACPRGHDR, *PIACPRGHDR;



#define ID_INC 0x00434e49	//"INC"49,4e,43,00
#define ID_IAC 0x00434149   //"IAC"49,41,43,00
#define DNLD_TYPE_BOOT1 	1
#define DNLD_TYPE_EBOOT 	2
#define DNLD_TYPE_CODE		3
#define DNLD_TYPE_GSM 		4
#define DNLD_TYPE_PHS 		5
#define DNLD_TYPE_DIAG 		6

#define DNLD_TYPE_TOC 	    7
#define DNLD_TYPE_SYSINFO 	8

#define DNLD_TYPE_END       0xff

//Flag1 define 
//bit1 calibration flag
#define NOCLEARDATA   0
#define CLEARDATA     1

#define MAX_IMG_NUM (6)
typedef struct{
	UINT32 type;
	UINT32 size;
	UINT8 *ver;
	UINT8  flag1;
	UINT8  flag2;
	UINT32 start;
	
	UINT32 date;
	UINT32 time;
	UINT32 sum;
	
}img_t;

//aaa
#define SYSINFO_SIGNATURE          0x4f464e49 //'INFO' 
#define BT_SIGNATURE               0x20205442 //'BT  ' 
#define TOUCH_SIGNATURE            0x20205054 //'TP  ' 
#define CLEANBOOT_SIGNATURE        0x54424c43 //'CLBT' 

#define SYSINFO_BLOCK              4  

typedef struct _info {
    UINT32    dwSignature;
	UINT8     reserve1[64-4];
	
	UINT8     flashbinVer[8];
	UINT32    flashbinDate;
	UINT32    flashbinTime;
	UINT32    flashbinSum;
	UINT8     reserve2[64-8-4-4-4];

	UINT8     ebootVer[8];
	UINT32    ebootSum;
	UINT8     reserve3[64-8-4];

	UINT8     boot1Ver[8];
	UINT32    boot1Sum;
	UINT8     reserve4[64-8-4];

	UINT8     diagVer[8];
	UINT32    diagSum;
	UINT8     reserve5[64-8-4];

	UINT32    touch_signature;
	UINT8     touch_data[64-4];

	UINT32    bt_signature;
	UINT8    bt_data[64-4];
		
	UINT8    reserve6[64-4*5];
	UINT32    dwAFinished;
	UINT32    dwCallLogCount;
	UINT32	  dwCleanBootFlag;		
	UINT32    fa_save_rtc;
	UINT32    phs_diag_power;
} SYSINFO;           // 512 bytes

extern INT32 ParserImg( UINT8 * img_ptr, INT32 img_len, img_t *imgs );
//extern void make_ecc_512(uint8_t * ecc_buf, uint8_t * data_buf);
