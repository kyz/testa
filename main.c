#include "stdio.h"
#include "stdlib.h"
#include "img.h"


char * usage="Usage:  gen_nand_img  <inputfilename> [outputfilename]";
#define NAND_CHIP_SIZE (1024*256*(512+16))

int main( int argc, char ** argvs )
{
	char bin_name[100];
	char img_name[100];
	char tmp_name[100];
	FILE *fd;
	char *in_data;
	unsigned long in_len;
	int i;
	unsigned int sum, chip_sum;
	unsigned char ch;
	unsigned long file_size=0;

	img_t imgs[8];
	
	if(argc >= 3 ){
		strcpy( bin_name, argvs[1] );
		strcpy( img_name, argvs[2] );
	}else if(argc==2){
		strcpy( bin_name, argvs[1] );
		strcpy( img_name, bin_name);
	}else { 
		printf( "parameter error!\n" );
		printf( "%s", usage );
		exit(0);
	}
	sprintf( tmp_name,"%s.tmp", bin_name );

	fd = fopen( bin_name, "rb" );

	if( !fd ){
		printf( "open file [%s] fail!\n", bin_name );
		exit(0);
	}
	fseek( fd, 0, SEEK_END );
	file_size = ftell( fd );
	printf( "input file size:%d\n", file_size );
	in_data = malloc( file_size );
	if( !in_data ){
		printf( "out of memory\n" );
		exit(0);
	}

	fseek(fd, 0, SEEK_SET );
	printf( "ftell:%d\n", ftell(fd) );
			
	in_len = fread( in_data, 1, file_size, fd );
	printf( "fread [%s] len:%d\n", bin_name, in_len );
	
	fclose( fd );

	printf( "parser file...\n");
	ParserImg(in_data,in_len,imgs);

	fd = fopen( tmp_name, "wb" );
	
	if( !fd ){
		printf( "open file %s fail!\n", tmp_name );
		exit(0);
	}

	printf( "write image to file: %s...\n", tmp_name );
	WriteImg(imgs, fd);
	fclose(fd);

	printf( "calc file sum...\n" );
	fd = fopen( tmp_name, "rb" );
	if( !fd ){
		printf( "open file %s fail!\n", tmp_name );
		exit(0);
	}
	sum =0;
	while( 1 ){
		if( !fread( &ch, 1, 1, fd ))
			break;
		sum += ch;
	}
	file_size=ftell(fd);
	printf( "file_size is: %d\n", file_size);
	fclose(fd);
	printf( "sum is %x\n", sum );
	chip_sum=0;
	chip_sum = sum + 0xff* (NAND_CHIP_SIZE-file_size);
	printf( "chip sum is %x\n", chip_sum );

	sprintf( img_name, "%s_%04X_%04X.bin", img_name, sum&0xffff, chip_sum&0xffff );
	rename( tmp_name, img_name );
	printf( "rename [%s] --> [%s]\n", tmp_name, img_name );

	return 0;
}


#if 0
void check_ecc_test( void )
{
		FILE *fp;
		unsigned char sect_data[536], ecc[4];
		int i;


		fp=fopen( "dump.bin", "rb" );

		for( i=0; i<256; i++ ){
			fread( sect_data, 536, 1, fp );
			make_ecc_512( ecc, sect_data );
			ecc[3] = make_fourth_ecc_512( sect_data );

			printf( "%d read ecc[ %x,%x,%x,%x]\n",i,  sect_data[520], sect_data[521], sect_data[522], sect_data[523] );
			printf( "%d calc ecc[ %x,%x,%x,%x]\n\n",i,  ecc[0], ecc[1], ecc[2], ecc[3] );

		}
		fclose(fp);
}
#endif

