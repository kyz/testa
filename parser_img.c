#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "img.h"



static UINT32 ChkSum(UINT8 *pBuffer, UINT32 length)
{
    UINT32 sum = 0;
    UINT32 nLen = length;

    while (nLen -- > 0) sum += *pBuffer++;
	
    return 0x10000-(sum&0xffff);
}

static	SYSINFO info;

INT32 ParserImg( UINT8 * img_ptr, INT32 img_len, img_t *imgs )
{
	IACPRGHDR * pHdr;
	
	UINT32 img_count=0;
	UINT32 offset = 0;
	UINT32 i;

	IACBINHDR *binHdr;

	memset( &info, 0xff, sizeof(info) );
	info.dwSignature = SYSINFO_SIGNATURE;
	info.dwCleanBootFlag = CLEANBOOT_SIGNATURE;


	pHdr = ( IACPRGHDR * )img_ptr;
	


	if( pHdr->ID == ID_IAC ){
		offset = (UINT32)img_ptr+sizeof(IACPRGHDR ) + pHdr->count* sizeof( IACBINHDR );
		
		for( i=0; i< pHdr->count; i++ ){
			binHdr = ( IACBINHDR *)(img_ptr+sizeof( IACPRGHDR ) + i * sizeof(IACBINHDR ) );

			switch( binHdr->type ){
				case DNLD_TYPE_BOOT1:
					img_count = 0;
					strcpy(info.boot1Ver,binHdr->version);
					info.boot1Sum = ChkSum( (UINT8*) offset, binHdr->size );
					break;
				case DNLD_TYPE_EBOOT:
					strcpy(info.ebootVer,binHdr->version);
					info.ebootSum =  ChkSum( (UINT8*) offset, binHdr->size );
					img_count = 2;
					break;
				case DNLD_TYPE_DIAG:
					img_count = 4;
					strcpy(info.diagVer,binHdr->version);
					info.diagSum = ChkSum( (UINT8*) offset, binHdr->size );
					break;
				case DNLD_TYPE_CODE:
					img_count = 5;
					strcpy(info.flashbinVer,binHdr->version);
					info.flashbinSum = ((PRGHDR*)offset)->chksum;
					info.flashbinDate = ((PRGHDR*)offset)->date;
					info.flashbinTime = ((PRGHDR*)offset)->time;
					break;
			}

			imgs[img_count].type = binHdr->type;
			imgs[img_count].size = binHdr->size;
			imgs[img_count].flag1 = pHdr->flag1;
			imgs[img_count].flag2 = pHdr->flag2;
			imgs[img_count].start = offset;
			imgs[img_count].ver = binHdr->version;

			if( imgs[img_count].type == DNLD_TYPE_CODE )
			{
				int len=0;
				imgs[img_count].size -=  sizeof( PRGHDR );
				imgs[img_count].sum = ((PRGHDR*)imgs[img_count].start)->chksum;
				imgs[img_count].date = ((PRGHDR*)imgs[img_count].start)->date;
				imgs[img_count].time = ((PRGHDR*)imgs[img_count].start)->time;
				imgs[img_count].start +=  sizeof( PRGHDR );

				PrepareFlashBin( imgs[img_count].start, imgs[img_count].size, &len );
				imgs[img_count].size = len;
			}
			

			offset += binHdr->size;
		}
			
	}else{
		printf( "ERR:  Invalid image!\n");
		exit(0);
	}

	//TOC BLK 1
	imgs[1].type = DNLD_TYPE_TOC;
	imgs[1].start = 0;
	imgs[1].size = 0;

	//SYSINFO BLK 4,5
	imgs[3].type = DNLD_TYPE_SYSINFO;
	imgs[3].start = (UINT32) &info;
	imgs[3].size = sizeof(info);
	
	imgs[6].type = DNLD_TYPE_END;


	printf( "\nver info:\n");
	printf( "Boot1 ver: %s\n", info.boot1Ver);
	printf( "Boot1 sum: %x\n", info.boot1Sum);
	printf( "Boot2 ver: %s\n", info.ebootVer);
	printf( "Boot2 sum: %x\n", info.ebootSum);
	printf( "Diag ver:  %s\n", info.diagVer);
	printf( "Diag sum:  %x\n", info.diagSum);
	printf( "MainCode ver: %s\n", info.flashbinVer);
	printf( "MainCode sum: %x\n\n", info.flashbinSum);

	return 1;
}



INT32 PrepareFlashBin( UINT32 img_start, UINT32 img_len, UINT32 * actual_img_len  )
{
	UINT8 *pSrc, *pDst;
	UINT32 addr,len,sum;
	UINT32 count=0;
	
	pSrc = (UINT8*)img_start;

	if( strncmp( pSrc, "B000FF\x0A", 7 ) ){
		//error
		return 0;
	}
	
	pSrc += 7;
//	memcpy( pDst, pSrc, 4 );
	pDst = (UINT8 * )img_start;

	pSrc+=4;
//	memcpy( actual_img_len, pSrc, 4 );
	pSrc+=4;


	count = 7+4+4;
	*actual_img_len = 0;
	while( count < img_len )
	{
		memcpy( &addr, pSrc, 4 );
		pSrc+=4;
		memcpy( &len, pSrc, 4 );
		pSrc+=4;
		memcpy( &sum, pSrc, 4 );
		pSrc+=4;

		memcpy( pDst, pSrc, len );
		count += len+4+4+4;
		pDst += len;
		pSrc += len;
		*actual_img_len += len;
	}

	
	return 1;	
}



int write_data(FILE *fd,  unsigned char *data, unsigned long data_size, int si )
{
	unsigned char sect_info[16];
	unsigned char * sect_ptr;
	unsigned int write_count, wr_len;
	unsigned char ecc[3];

	memset( sect_info, 0xff, sizeof(sect_info));
	write_count = 0;
	sect_ptr = data;

	while( write_count < data_size ){

		make_ecc_512(ecc, sect_ptr);
		memset( sect_info, 0xff, 16);
		sect_info[8]=ecc[0];
		sect_info[9]=ecc[1];
		sect_info[10]=ecc[2];
//		if( ecc[0]==0xff && ecc[1]==0xff && ecc[2]==0xff )
//			sect_info[11]=0xff;
//		else
//			sect_info[11]=make_fourth_ecc_512(sect_ptr);


		if( !si ){
			sect_info[4]=0xfc;
			sect_info[5]=0; 
		}else{
#if 1
//small block nand		
			//small block Nand, swap wReserved2[0],[1]
			char tmp;
			memcpy( sect_info, sect_ptr+512, 8 );
			tmp = sect_info[6];
			sect_info[6]=sect_info[7];
			sect_info[7]=tmp;
#else
//large block nand.
#endif
		}
		fwrite( sect_ptr, 512, 1, fd );
		fwrite( sect_info, 16, 1, fd );
		if( si ){
			write_count += 520;
			sect_ptr += 520;
		}else{
			write_count += 512;
			sect_ptr += 512;
		}
	}
	return 1;
}

unsigned char buffer[512*256*12];

int WriteImg( img_t *imgs, FILE *fd )
{
	int i=0;



//	sect_info = &sect_data[512]
	
//1. write boot1 [ BLOCK 0 ]
	memset( buffer, 0xff, 256*512 );
	memcpy( buffer, (char *)(imgs[0].start), imgs[0].size );
	write_data( fd, buffer, 256*512, 0 );

//2. write TOC [BLOCK 1 ]
	memset( buffer, 0xff, 256*512 );
	write_data( fd, buffer, 256*512, 0 );

//3. write eboot [BLOCK 2,3]
	memset( buffer, 0xff, 256*512*2 );
	memcpy( buffer, (char *)(imgs[2].start), imgs[2].size );
	write_data( fd, buffer, 256*512*2, 0 );

//4. write sysinfo [BLOCK 4, 5 ]
	memset( buffer, 0xff, 256*512*2 );
	memcpy( buffer, (char *)(imgs[3].start), imgs[3].size );
	write_data( fd, buffer, 256*512*2, 0 );


//5. write diag [BLOCK 6--17]
	memset( buffer, 0xff, 256*512*12 );
	memcpy( buffer, (char *)(imgs[4].start), imgs[4].size );
	write_data( fd, buffer, 256*512*12, 0 );


//6. write main code [BLOCK 18-- ]
	write_data( fd, (unsigned char *)(imgs[5].start), imgs[5].size, 1 );

	return 0;
}

