/******************************************************************************
**
**  COPYRIGHT (C) 2002-2003 Centrality Communications, Inc.
**
**  FILENAME:       smecc.c
**
**  PURPOSE:        This file contains the SMC ECC Encode && Decode
**
**  2003-05-12
******************************************************************************/

#include "smecc.h"

/////////////////////////////////////////////////////////////////
///from samsung for 512_ecc

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*		make_ecc_512                                                         */
/* DESCRIPTION                                                               */
/*		This function generates 3 byte ECC for 512 byte data.                */
/*      (Software ECC)                                                       */
/* PARAMETERS                                                                */
/*		ecc_buf			the location where ECC should be stored              */
/*		data_buf		given data                                           */
/* RETURN VALUES                                                             */
/*		none                                                                 */
/*                                                                           */
/*****************************************************************************/
void make_ecc_512(uint8_t * ecc_buf, uint8_t * data_buf)
{
	
    uint32_t	i, ALIGN_FACTOR; 
	uint32_t	tmp;
	uint32_t	uiparity = 0;
	uint32_t	parityCol, ecc = 0;
	uint32_t	parityCol4321 = 0, parityCol4343 = 0, parityCol4242 = 0, parityColTot = 0;
	uint32_t	*Data;
	uint32_t	Xorbit=0;

	ALIGN_FACTOR = (uint32_t)data_buf % 4 ;
	Data = (uint32_t *)(data_buf + ALIGN_FACTOR);

	for( i = 0; i < 16; i++)
	{
		parityCol = *Data++; 
		tmp = *Data++; parityCol ^= tmp; parityCol4242 ^= tmp;
		tmp = *Data++; parityCol ^= tmp; parityCol4343 ^= tmp;
		tmp = *Data++; parityCol ^= tmp; parityCol4343 ^= tmp; parityCol4242 ^= tmp;
		tmp = *Data++; parityCol ^= tmp; parityCol4321 ^= tmp;
		tmp = *Data++; parityCol ^= tmp; parityCol4242 ^= tmp; parityCol4321 ^= tmp;
		tmp = *Data++; parityCol ^= tmp; parityCol4343 ^= tmp; parityCol4321 ^= tmp;
		tmp = *Data++; parityCol ^= tmp; parityCol4242 ^= tmp; parityCol4343 ^= tmp; parityCol4321 ^= tmp;

		parityColTot ^= parityCol;

		tmp = (parityCol >> 16) ^ parityCol;
		tmp = (tmp >> 8) ^ tmp;
		tmp = (tmp >> 4) ^ tmp;
		tmp = ((tmp >> 2) ^ tmp) & 0x03;
		if ((tmp == 0x01) || (tmp == 0x02))
		{
			uiparity ^= i;
			Xorbit ^= 0x01;
		}
	}

	tmp = (parityCol4321 >> 16) ^ parityCol4321;
	tmp = (tmp << 8) ^ tmp;
	tmp = (tmp >> 4) ^ tmp;
	tmp = (tmp >> 2) ^ tmp;
	ecc |= ((tmp << 1) ^ tmp) & 0x200;	// p128

	tmp = (parityCol4343 >> 16) ^ parityCol4343;
	tmp = (tmp >> 8) ^ tmp;
	tmp = (tmp << 4) ^ tmp;
	tmp = (tmp << 2) ^ tmp;
	ecc |= ((tmp << 1) ^ tmp) & 0x80;	// p64

	tmp = (parityCol4242 >> 16) ^ parityCol4242;
	tmp = (tmp >> 8) ^ tmp;
	tmp = (tmp << 4) ^ tmp;
	tmp = (tmp >> 2) ^ tmp;
	ecc |= ((tmp << 1) ^ tmp) & 0x20;	// p32

	tmp = parityColTot & 0xFFFF0000;
	tmp = tmp >> 16;
	tmp = (tmp >> 8) ^ tmp;
	tmp = (tmp >> 4) ^ tmp;
	tmp = (tmp << 2) ^ tmp;
	ecc |= ((tmp << 1) ^ tmp) & 0x08;	// p16

	tmp = parityColTot & 0xFF00FF00;
	tmp = (tmp >> 16) ^ tmp;
	tmp = (tmp >> 8);
	tmp = (tmp >> 4) ^ tmp;
	tmp = (tmp >> 2) ^ tmp;
	ecc |= ((tmp << 1) ^ tmp) & 0x02;	// p8

	tmp = parityColTot & 0xF0F0F0F0 ;
	tmp = (tmp << 16) ^ tmp;
	tmp = (tmp >> 8) ^ tmp;
	tmp = (tmp << 2) ^ tmp;
	ecc |= ((tmp << 1) ^ tmp) & 0x800000;	// p4

	tmp = parityColTot & 0xCCCCCCCC ;
	tmp = (tmp << 16) ^ tmp;
	tmp = (tmp >> 8) ^ tmp;
	tmp = (tmp << 4) ^ tmp;
	tmp = (tmp >> 2);
	ecc |= ((tmp << 1) ^ tmp) & 0x200000;	// p2

	tmp = parityColTot & 0xAAAAAAAA ;
	tmp = (tmp << 16) ^ tmp;
	tmp = (tmp >> 8) ^ tmp;
	tmp = (tmp >> 4) ^ tmp;
	tmp = (tmp << 2) ^ tmp;
	ecc |= (tmp & 0x80000);	// p1

	ecc |= (uiparity & 0x01) <<11;	
	ecc |= (uiparity & 0x02) <<12;	
	ecc |= (uiparity & 0x04) <<13;
	ecc |= (uiparity & 0x08) <<14;

	if (Xorbit)
	{
		ecc |= (ecc ^ 0x00AAAAAA)>>1;
	}
	else
	{
		ecc |= (ecc >> 1);
	}

	ecc = ~ecc;
	*(ecc_buf + 2) = (uint8_t) (ecc >> 16);
	*(ecc_buf + 1) = (uint8_t) (ecc >> 8);
	*(ecc_buf + 0) = (uint8_t) (ecc);
}


unsigned char make_fourth_ecc_512( uint8_t * data_buf)
{
	unsigned long tmp=0, ecc =0, *ptr;

	int i;
	
	ptr = (unsigned long *)data_buf;
	for( i=0; i<512/4; i++ )
		tmp^=*ptr++;

	tmp = (tmp >> 16) ^ tmp;
	tmp = (tmp >> 8) ^ tmp;
	tmp = (tmp >> 4) ^ tmp;
	tmp = (tmp >> 2) ^ tmp;
	tmp = (tmp >>1 ) ^ tmp;
	tmp &=1;
	if( tmp&1)
		ecc=0xa0;
	else
		ecc=0xf0;

//	printf( "fourth ecc byte=%x\n", ecc );
	return ecc;
}
